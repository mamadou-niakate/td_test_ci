from CartePizzeriaException import CartePizzeriaException


class CartePizzeria:

    def __init__(self, pizza_list=[]):
        self.carte = pizza_list

    """ Retourne un booléen indiquant si la carte est vide ou non """

    def is_empty(self):
        return len(self.carte) == 0

    """retourne le nombre de pizzas de la carte"""

    def nb_pizza(self):
        return len(self.carte)

    """ajoute une pizza à la carte"""

    def add_pizza(self, pizza):
        self.carte.append(pizza)

    """retire la pizza nommée `name` de la carte, si celle-ci n'existe pas, lève une exception 
    `CartePizzeriaException` """

    def remove_pizza(self, name):
        if self.carte.index(name):
            self.carte.remove(name)
        else:
            raise CartePizzeriaException()


if __name__ == "__main__":
    print("Hello world")
