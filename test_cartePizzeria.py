import unittest
from unittest.mock import Mock
from CartePizzeria import CartePizzeria


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.carte_pizzeria = CartePizzeria(["Mexicaine", "Mozzarella", "Napolitaine"])

    def test_number_of_pizza(self):
        carte_pizzeria = CartePizzeria()
        pizza = Mock()
        carte_pizzeria.carte = [pizza]
        self.assertEqual(carte_pizzeria.nb_pizza(), len(carte_pizzeria.carte))

    def test_cart_is_empty(self):
        carte_pizzeria = CartePizzeria()
        pizza = Mock()
        carte_pizzeria.carte = [pizza]
        self.assertEqual(carte_pizzeria.is_empty(), False)

    def test_add_pizza(self):
        carte_pizzeria = CartePizzeria()
        pizza = Mock()
        carte_pizzeria.carte = [pizza]
        pizza1 = Mock()
        carte_pizzeria.add_pizza(pizza1)
        self.assertEqual(len(carte_pizzeria.carte), len(carte_pizzeria.carte))

    def test_remove_pizza(self):
        carte_pizzeria = CartePizzeria()
        pizza1 = Mock()
        pizza2 = Mock()
        carte_pizzeria.carte = [pizza1, pizza2]
        carte_pizzeria.remove_pizza(pizza1)
        self.assertEqual(len(carte_pizzeria.carte), len(carte_pizzeria.carte))


if __name__ == '__main__':
    unittest.main()
